<?php
/**
 * @file
 * API reference for RESTful web services entity references.
 *
 * This file contains no working PHP code; it exists to provide additional
 * documentation for doxygen as well as to document hooks in the standard Drupal
 * manner.
 */

/**
 * Alter response data for a loaded entity.
 *
 * Provides an advanced hook for altering response data with the controller,
 * wrapper, and response format all provided for convenience. Otherwise it is no
 * different from the standard response alter hook provided by restws.
 *
 * This hook operates only on entities derived from an entity reference field.
 *
 * WARNING: Beware which properties you add to the data array from the wrapper.
 * While data has already been checked for security, the wrapper contains all
 * properties whether they should be accessible or not. It is advisable to check
 * that sensitive data is injected inadvertently.
 *
 * @param array $data
 *   The data to alter. This corresponds to a single entity loaded from an
 *   entity reference field on some parent entity.
 * @param RestWSResourceControllerInterface $controller
 *   The resource controller for the given entity.
 * @param EntityStructureWrapper $wrapper
 *   The entity metadata wrapper for the given entity.
 * @param RestWSFormatInterface $format
 *   The format used in this response (JSON, XML, etc).
 */
function hook_restws_entityreference_entity_data_alter(&$data, RestWSResourceControllerInterface $controller, EntityStructureWrapper $wrapper, RestWSFormatInterface $format) {
  $data['my_data'] = 'mydata';
}
